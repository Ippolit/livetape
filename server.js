const fs = require('fs');
const http = require('http');
const path = require('path');
const redis = require('redis');
const Pool = require('pg-pool');
const dotenv = require('dotenv');
const express = require('express');
const socket = require('socket.io');
const { v4: uuidv4 } = require('uuid');
const compress = require('compression');
const favicon = require('serve-favicon');
const session = require('express-session');
const connectRedis = require('connect-redis');
const cookieParser = require('cookie-parser');
const html_sanitizer = require('sanitize-html');
const fileUpload = require('express-fileupload');

const FOLDER_VIEWS = path.join(__dirname, 'dist');
const FOLDER_UPLOADS = path.join(__dirname, 'uploads');
const FOLDER_AVATARS = path.join(__dirname, 'avatars');
const PAGE_LIMIT_MESSAGE = 10;

fs.access(FOLDER_UPLOADS, fs.constants.F_OK, (err) => {
  if (err) fs.mkdirSync(FOLDER_UPLOADS);
});

const app = express();

dotenv.config();
const server = http.createServer(app);
const io = socket(server);
const port = process.env.PORT;
const hostname = process.env.HOSTNAME;
const db = new Pool({
  database: process.env.DB_DATABASE,
  host: process.env.DB_HOST,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT,
  user: process.env.DB_USER,
  connectionTimeoutMillis: 1000,
  idleTimeoutMillis: 300000,
  max: 20,
});

const get_user_async = async function get_user_async(id) {
  try {
    return (await db.query(`
      SELECT id, person_fio_full, person_fio_short, avatar_link
      FROM staff.view_person_lite_simple 
      WHERE id = $1`,
    [id])).rows[0];
  } catch (e) {
    console.log(e.message);
    return null;
  }
};
const validate_user = async function validate_user(req, res, next) {
  if (req.session.person && req.session.person.id) {
    const person = await get_user_async(req.session.person.id);
    if (person && person.id) {
      req.session.person.avatar_link = person.avatar_link;
      req.session.person.person_fio_full = person.person_fio_full;
      req.session.person.person_fio_short = person.person_fio_short;
      next();
    }
  } else {
    res.redirect('/auth');
  }
};
const availableParam = function availableParam(obj, ...params) {
  return obj && params && params.length !== 0 && params.every((p) => p in obj);
};
const html2plain = function html2plaintext(html) {
  return [...html.matchAll(/<[^>]+>([^<>]+)/gim)].map((x) => x[1]).join(' ');
};
const isEmptyString = /^\s*$/;
// Обрезка лишних пробелов в контенте
const truncateContent = /^(<p><br><\/p>)*(.*?)(<p><br><\/p>)*$/gmi;
const expressCookieParser = cookieParser(process.env.SECRET);
const RedisStore = connectRedis(session);
const redisClient = redis.createClient();
const sessionStore = new RedisStore({ client: redisClient });
const sessionMiddleWare = session({
  genid: () => uuidv4(),
  store: sessionStore,
  resave: false,
  saveUninitialized: true,
  secret: process.env.SECRET,
  cookie: {
    maxAge: 256000 * 10000,
  },
  key: process.env.COOKIE_NAME,
});

const SANITIZE_CONFIG = {
  allowedTags: [
    // элементы
    'a', // ссылка
    'p', // параграф
    'br', // перенос строки
    'hr', // горизонтальная линия
    'img', // изображение
    'code', // возможные вставки и оформления кода
    'span', // строчный элемент
    'iframe', // встроенный контент

    // оформление текста
    'em', // косой
    'del', // зачёркнут
    'font', // шрифт
    'strong', // жирный

    // списки
    'li', // пункт
    'ol', // нумерованный
    'ul', // маркированный

    // индексы
    'sub', // нижний
    'sup', // верхний
  ],
  allowedAttributes: {
    '*': ['style'],
    p: ['align'],
    ul: ['type'],
    br: ['clear'],
    li: ['type', 'value'],
    font: ['color', 'face', 'size'],
    ol: ['type', 'reversed', 'start'],
    a: ['href', 'title', 'target', 'type'],
    hr: ['align', 'color', 'noshade', 'size', 'width'],
    img: ['align', 'alt', 'border', 'longdesc', 'lowsrc', 'src', 'vspace'],
    iframe: ['align', 'allowfullscreen', 'frameborder', 'height', 'src', 'width'],
  },
  // исключается встраивание изображений base64 - data:
  allowedSchemes: ['http', 'https'],
};

app.use(compress());
app.use(fileUpload());
app.use(sessionMiddleWare);
app.use(expressCookieParser);
app.use(express.urlencoded({
  extended: true,
  limit: '500mb',
  parameterLimit: 10000,
}));
app.use(express.json({
  extended: true,
  limit: '500mb',
}));
app.use('/js', express.static(path.join(FOLDER_VIEWS, 'js')));
app.use('/css', express.static(path.join(FOLDER_VIEWS, 'css')));
app.use('/asset', express.static(path.join(FOLDER_VIEWS, 'asset')));
app.use('/fonts', express.static(path.join(FOLDER_VIEWS, 'fonts')));
app.use(favicon(`${__dirname}/favicon.ico`));

const connectedSockets = [];
io.on('connect', (sckt) => {
  expressCookieParser(sckt.handshake, {}, () => {
    sessionStore.get(sckt.handshake.signedCookies[process.env.COOKIE_NAME], (err, sessionData) => {
      if (sessionData != null) sckt.session = sessionData;
      connectedSockets.push(sckt);
    });
  });
  sckt.on('disconnect', () => {
    connectedSockets.splice(connectedSockets.indexOf(sckt), 1);
  });
});

app.post('/api/send_attach', validate_user, (req, res) => {
  if (!availableParam(req.files, 'filepond')) {
    return res.status(400).send('Не корректный запрос [Bad request]');
  }

  const uuid_name = uuidv4();

  if (req.files.filepond) {
    req.files.filepond.mv(
      path.join(FOLDER_UPLOADS, uuid_name),
      (err) => {
        if (err) return res.status(400).send('Не удалось загрузить файл [Couldn\'t upload file]');
        return res.send(uuid_name);
      },
    );
  }

  return 'Ok';
});
app.delete('/api/send_attach', validate_user, (req, res) => {
  res.send('Ok');
});

app.get('/api/get_attach/:message_id/:file_id', validate_user, async (req, res) => {
  if (!availableParam(req.params, 'message_id', 'file_id')) {
    return res.status(400).send('Не корректный запрос [Bad request]');
  }

  try {
    const message = (await db.query('SELECT attached FROM message WHERE id = $1', [req.params.message_id])).rows[0];
    if (!message) {
      console.log('Сообщение не существует');
      return 'Err';
    }
    if (!availableParam(message, 'attached')) {
      console.log('Прикреплённых файлов нет');
      return 'Err';
    }

    const file = message.attached.find((a) => a.id === req.params.file_id);
    if (!file) {
      console.log('Файл не указан в сообщении');
      return 'Err';
    }

    const pathFile = path.join(FOLDER_UPLOADS, file.id);

    fs.promises
      .access(pathFile, fs.constants.F_OK)
      .then(() => res.download(pathFile, file.name))
      .catch(() => res.status(404).send('Файл не найден [File not found]'));
  } catch (e) {
    console.log(e.message);
    res.status(404).send('Файл не найден [File not found]');
  }

  return 'Ok';
});

app.post('/api/send_inner_attach', validate_user, (req, res) => {
  if (!availableParam(req.files, 'uf')) {
    return res.status(400).send('Не корректный запрос [Bad request]');
  }

  const uuid_name = uuidv4();
  if (req.files.uf) {
    req.files.uf.mv(
      path.join(FOLDER_UPLOADS, uuid_name),
      (err) => {
        if (err) return res.status(400).send('Не удалось загрузить файл [Couldn\'t upload file]');
        return res.send({
          success: true,
          file_name: req.files.uf.name,
          file_link: `/api/get_inner_image_attach/${uuid_name}/${req.files.uf.name}`,
          file: `/api/get_inner_image_attach/${uuid_name}/${req.files.uf.name}`,
        });
      },
    );
  }

  return 'Ok';
});
app.get('/api/get_inner_image_attach/:file_id/:file_name', validate_user, async (req, res) => {
  if (!(availableParam(req.params, 'file_id')
        && availableParam(req.params, 'file_name'))) {
    return res.status(400).send('Не корректный запрос [Bad request]');
  }

  try {
    const pathFile = path.join(FOLDER_UPLOADS, req.params.file_id);

    fs.promises
      .access(pathFile, fs.constants.F_OK)
      .then(() => fs.createReadStream(pathFile).pipe(res))
      .catch(() => res.status(404).send('Файл не найден [File not found]'));
  } catch (e) {
    console.log(e.message);
    res.status(404).send('Файл не найден [File not found]');
  }

  return 'Ok';
});

app.post('/api/send_comment', validate_user, async (req, res) => {
  if (!availableParam(req.body, 'content', 'parent_id')
      || isEmptyString.test(req.body.content)) {
    return res.status(400).send('Не корректный запрос [Bad request]');
  }

  const person = req.session.person;
  const clean_content = html_sanitizer(req.body.content, SANITIZE_CONFIG);
  const comment = {
    person: {
      avatar_link: person.avatar_link,
      person_fio_short: person.person_fio_short,
    },
    parent_id: req.body.parent_id,
    content: clean_content,
    attached: req.body.attached,
    datetime: new Date(),
    my_message: false,
    plain_content: html2plain(clean_content),
  };

  try {
    // получение информации о владельце поста и шаринге
    const parent_post = (await db.query(`
        WITH RECURSIVE parents AS (
          SELECT m.person_id,
                 m.id,
                 m.parent_id
          FROM message m
          WHERE m.id = $1
          UNION ALL
          SELECT m.person_id,
                 m.id,
                 m.parent_id
          FROM message m
                 JOIN parents ON parents.parent_id = m.id
        )
        SELECT parents.person_id,
               (SELECT jsonb_agg(jsonb_build_object('id', sm.person_id))
                FROM shared_message sm
                WHERE sm.message_id = parents.id
               ) AS shared
        FROM parents
        WHERE parent_id ISNULL`,
    [comment.parent_id])).rows[0];

    const comment_info = (await db.query(`
      INSERT INTO message (person_id, parent_id, content, plain_content, post_stamp, attached)
      VALUES ($1, $2, $3, $4, $5, $6)
      RETURNING id`,
    [
      person.id,
      comment.parent_id,
      comment.content,
      comment.plain_content,
      comment.datetime,
      comment.attached === null ? null : JSON.stringify(comment.attached),
    ])).rows[0];

    comment.id = comment_info.id;
    // comment.shared = comment_info.shared;

    connectedSockets.forEach((sckt) => {
      const socket_person_id = sckt.session && sckt.session.person && sckt.session.person.id;
      if (parent_post.shared === null || parent_post.shared.length === 0 // нет шаринга
          || (socket_person_id // проверка аутентифицированного сокета
              && (socket_person_id === person.id // сокет - отправитель
                  || socket_person_id === parent_post.person_id // сокет - владелец поста
                  || parent_post.shared.some((s) => s.id === socket_person_id)))) { // сокет относится к расшаренным
        comment.my_message = socket_person_id === req.session.person.id;
        io.to(sckt.id).emit('append_comment', comment);
      }
    });

    return res.status(200).send('Ok');
  } catch (e) {
    console.log(e);
    return res.status(400).send('Не удалось отправить комментарий [Bad request]');
  }
});

app.post('/api/send_message', validate_user, async (req, res) => {
  if (!availableParam(req.body, 'content')
      || isEmptyString.test(req.body.content)) {
    return res.status(400).send('Не корректный запрос [Bad request]');
  }

  const person = req.session.person;
  const clean_content = html_sanitizer(req.body.content, SANITIZE_CONFIG);
  // с клиента приходит строка, а не булево значение
  // если галочки нет, то null - означает не отслеживать прочтение
  // false/true в БД будет означать не прочтено/прочтено
  const marked_state = req.body.control_reading_post === 'true' ? false : null;
  const message = {
    person: {
      avatar_link: person.avatar_link,
      person_fio_short: person.person_fio_short,
    },
    content: clean_content,
    plain_content: html2plain(clean_content),
    my_message: false,
    datetime: new Date(),
    attached: req.body.attached,
  };

  const client = await db.connect();
  try {
    message.shared = !req.body.shared ? null : req.body.shared.map((s) => ({ id: s.id }));
    // start transaction
    await client.query('BEGIN');
    message.id = (await client.query(`
      INSERT INTO message (person_id, content, plain_content, post_stamp, attached)
        VALUES ($1, $2, $3, $4, $5)
        RETURNING id`,
    [
      person.id,
      message.content,
      message.plain_content,
      message.datetime,
      message.attached === null ? null : JSON.stringify(message.attached),
    ])).rows[0].id;
    message.shared = (await client.query(`
      INSERT INTO shared_message(message_id, person_id, marked_state)
      SELECT $1, t.id, $2
      FROM (SELECT (t_json ->> 'id')::UUID AS id
            FROM jsonb_array_elements($3) t_json) t
      RETURNING person_id AS id, (SELECT system.make_fio_short(vp.person_lastname, vp.person_name,vp.person_fathername)
                                  FROM staff.tblperson vp
                                  WHERE person_id = vp.id) AS person_fio_short`,
    [
      message.id,
      marked_state,
      message.shared === null ? null : JSON.stringify(message.shared),
    ])).rows;
    await client.query('COMMIT');
    // end transaction

    connectedSockets.forEach((sckt) => {
      const socket_person_id = sckt.session && sckt.session.person && sckt.session.person.id;
      if (message.shared === null || message.shared.length === 0
          || (socket_person_id
              && (socket_person_id === person.id
                  || message.shared.some((s) => s.id === socket_person_id)))) { // получаетель сообщения
        message.my_message = socket_person_id === person.id;
        const result_message = {
          ...message,
          marked_state: message.my_message ? null : marked_state,
        };
        io.to(sckt.id).emit('append_post', result_message);
      }
    });

    return res.status(200).send('Ok');
  } catch (e) {
    await client.query('ROLLBACK');
    console.log(e);
    return res.status(400).send('Не удалось отправить сообщение [Bad request]');
  } finally {
    client.release();
  }
});

app.post('/api/change_message', validate_user, async (req, res) => {
  if (!availableParam(req.body, 'id', 'content')) {
    return res.status(400).send('Не корректный запрос [Bad request]');
  }

  try {
    const person = req.session.person;
    const clean_content = html_sanitizer(req.body.content, SANITIZE_CONFIG);
    const message = {
      id: req.body.id,
      content: clean_content,
      attached: req.body.attached || null,
      change_stamp: new Date(),
      plain_content: html2plain(clean_content),
    };

    // получение информации о владельце поста и шаринге
    const parent_post = (await db.query(`
        WITH RECURSIVE parents AS (
          SELECT m.person_id,
                 m.id,
                 m.parent_id
          FROM message m
          WHERE m.id = $1
          UNION ALL
          SELECT m.person_id,
                 m.id,
                 m.parent_id
          FROM message m
                 JOIN parents ON parents.parent_id = m.id
        )
        SELECT parents.person_id,
               (SELECT jsonb_agg(jsonb_build_object('id', sm.person_id))
                FROM shared_message sm
                WHERE sm.message_id = parents.id
               ) AS shared
        FROM parents
        WHERE parent_id ISNULL`,
    [message.id])).rows[0];

    const id = (await db.query(`
      UPDATE message
      SET content = $3,
          plain_content = $4,
          change_stamp = $5,
          attached = $6
      WHERE NOT deleted
          AND id = $2
          AND person_id = $1
      RETURNING id, parent_id`,
    [
      person.id,
      message.id,
      message.content,
      message.plain_content,
      message.change_stamp,
      message.attached === null ? null : JSON.stringify(message.attached),
    ])).rows[0].id;

    if (id) {
      connectedSockets.forEach((sckt) => {
        const socket_person_id = sckt.session && sckt.session.person && sckt.session.person.id;
        if (parent_post.shared === null || parent_post.shared.length === 0 // нет шаринга
            || (socket_person_id // проверка аутентифицированного сокета
                && (socket_person_id === person.id // сокет - отправитель
                    || socket_person_id === parent_post.person_id // сокет - владелец поста
                    || parent_post.shared.some((s) => s.id === socket_person_id)))) { // сокет относится к расшаренным
          message.my_message = socket_person_id === person.id;
          io.to(sckt.id).emit('update_post', message);
        }
      });
      res.status(200).send('Ok');
    }
  } catch (e) {
    console.log(e);
    res.status(400).send('Ошибка в изменении сообщения');
  }

  return 'Ok';
});

app.post('/api/mark_view_message', validate_user, async (req, res) => {
  if (!availableParam(req.body, 'id')) {
    return res.status(400).send('Не корректный запрос [Bad request]');
  }

  try {
    const person = req.session.person;
    const message_id = req.body.id;
    const time_stamp = new Date();

    const mark_message = (await db.query(`
      UPDATE shared_message
      SET marked_state = true,
          marked_date = $3
      WHERE person_id = $1
        AND message_id = $2
        AND marked_state = false
      RETURNING marked_state`,
    [
      person.id,
      message_id,
      time_stamp,
    ])).rows[0];

    if (mark_message.marked_state === true) {
      res.status(200).send(true);
    } else {
      res.status(200).send(false);
    }
  } catch (e) {
    console.log(e);
    res.status(400).send('Ошибка при изменении отметки прочтения');
  }

  return 'Ok';
});

app.post('/api/message_delete', validate_user, async (req, res) => {
  if (!availableParam(req.body, 'id')) {
    return res.status(400).send('Не корректный запрос [Bad request]');
  }

  const deleted_message_id = req.body.id;
  const person = req.session.person;

  try {
    // получение информации о владельце поста и шаринге
    const parent_post = (await db.query(`
        WITH RECURSIVE parents AS (
          SELECT m.person_id,
                 m.id,
                 m.parent_id
          FROM message m
          WHERE m.id = $1
          UNION ALL
          SELECT m.person_id,
                 m.id,
                 m.parent_id
          FROM message m
                 JOIN parents ON parents.parent_id = m.id
        )
        SELECT parents.person_id,
               (SELECT jsonb_agg(jsonb_build_object('id', sm.person_id))
                FROM shared_message sm
                WHERE sm.message_id = parents.id
               ) AS shared
        FROM parents
        WHERE parent_id ISNULL`,
    [deleted_message_id])).rows[0];

    // Удаление сообщения
    const date_stamp = new Date();
    const affected_row = (await db.query(`
      UPDATE message
      SET deleted           = true,
          deleted_timestamp = $3
      WHERE id  = $1 
          AND person_id = $2
          AND NOT deleted
      RETURNING id, parent_id`,
    [
      deleted_message_id,
      person.id,
      date_stamp,
    ])).rows[0];

    if (affected_row && affected_row.id === req.body.id) {
      // Рекурсивное удаление подчинённых комментариев
      db.query(`
        WITH RECURSIVE comments AS (
            SELECT m.id,
                   m.parent_id
            FROM message m
            WHERE m.parent_id = $1
            UNION ALL
            SELECT m.id,
                   m.parent_id
            FROM message m JOIN comments ON m.parent_id = comments.id
        )
        UPDATE message
        SET deleted           = true,
            deleted_timestamp = $2
        WHERE id IN (SELECT id FROM comments)`,
      [
        affected_row.id,
        date_stamp,
      ]).catch((err) => console.log(err));

      // трансляция определённому кругу лиц
      connectedSockets.forEach((sckt) => {
        const socket_person_id = sckt.session && sckt.session.person && sckt.session.person.id;
        if (parent_post.shared === null || parent_post.shared.length === 0 // нет шаринга
            || (socket_person_id // проверка аутентифицированного сокета
                && (socket_person_id === person.id // сокет - отправитель
                    || socket_person_id === parent_post.person_id // сокет - владелец поста
                    || parent_post.shared.some((s) => s.id === socket_person_id)))) { // сокет относится к расшаренным
          io.to(sckt.id).emit('delete_message', affected_row);
        }
      });
      res.status(200).send('Ok');
    } else {
      res.status(400).send('Сообщение не найдено');
    }
  } catch (e) {
    console.log(e);
    res.status(400).send('Не корректный запрос [Bad request]');
  }
  return 'Ok';
});

app.post('/api/get_messages_page', validate_user, async (req, res) => {
  if (!availableParam(req.body, 'page')
      || !Number.isInteger(+req.body.page)) {
    return res.status(400).send('Не корректный запрос [Bad request]');
  }

  try {
    const offset = req.body.page * PAGE_LIMIT_MESSAGE;
    const current_person_id = req.session.person.id;
    const messages = (await db.query(`
      SELECT (SELECT jsonb_build_object(
                       'avatar_link', tu.avatar_link,
                       'person_fio_short', system.make_fio_short(t1.person_lastname, t1.person_name, t1.person_fathername))
              FROM staff.tbluser tu
                     JOIN staff.tblperson t1 ON tu.id = t1.user_id
              WHERE t1.id = m.person_id) AS person,
             m.id,
             m.person_id,
             m.content,
             m.post_stamp,
             m.change_stamp,
             m.attached,
             (
               SELECT jsonb_agg(
                        jsonb_build_object(
                          'id', comments.id,
                          'parent_id', comments.parent_id,
                          'person', comments.person,
                          'person_id', comments.person_id,
                          'content', comments.content,
                          'post_stamp', comments.post_stamp,
                          'attached', comments.attached,
                          'child_count',
                          (SELECT COUNT(msg_p)
                           FROM message msg_p
                           WHERE comments.id = msg_p.parent_id
                             AND NOT msg_p.deleted)
                          )
                        )
               FROM (
                      SELECT (SELECT jsonb_build_object(
                                       'avatar_link', tu2.avatar_link,
                                       'person_fio_short',
                                       system.make_fio_short(t2.person_lastname, t2.person_name, t2.person_fathername))
                              FROM staff.tbluser tu2
                                     JOIN staff.tblperson t2 ON tu2.id = t2.user_id
                              WHERE t2.id = p_message.person_id) AS person,
                             p_message.id,
                             p_message.parent_id,
                             p_message.post_stamp,
                             p_message.person_id,
                             p_message.content,
                             p_message.attached
                      FROM (SELECT *
                            FROM message
                            WHERE m.id = message.parent_id
                              AND NOT message.deleted) p_message
                      ORDER BY p_message.post_stamp
                    ) comments
             )                           AS childs,
             (
               CASE
                 WHEN m.parent_id ISNULL THEN (
                   SELECT jsonb_agg(jsonb_build_object(
                     'person_id', sm2.person_id,
                     'marked_state', sm2.marked_state,
                     'person_fio_short',
                     (SELECT system.make_fio_short(vp3.person_lastname, vp3.person_name, vp3.person_fathername)
                      FROM staff.tblperson vp3
                      WHERE sm2.person_id = vp3.id)
                     ))
                   FROM shared_message sm2
                   WHERE sm2.message_id = m.id
                 )
                 END
               )                         AS shared,
             (
               CASE
                 WHEN m.parent_id ISNULL AND m.person_id != $3 THEN (
                   SELECT sm3.marked_state
                   FROM shared_message sm3
                   WHERE sm3.person_id = $3
                     AND sm3.message_id = m.id
                 )
                 END
               )                         AS marked_state
      FROM message m
      WHERE NOT m.deleted
        AND m.parent_id ISNULL
        AND (m.person_id = $3
        OR EXISTS(
               SELECT *
               FROM shared_message sm1
               WHERE sm1.message_id = m.id
                 AND sm1.person_id = $3
               )
        OR (NOT EXISTS(SELECT * FROM shared_message sm WHERE sm.message_id = m.id)))
      ORDER BY m.post_stamp DESC
      LIMIT $1 OFFSET $2`,
    [
      PAGE_LIMIT_MESSAGE,
      offset,
      current_person_id,
    ])).rows;
    // console.timeEnd('pages');

    messages.forEach((m) => {
      m.my_message = m.person_id === req.session.person.id;
      // delete m.user_id;
      if (m.childs && m.childs.length !== 0) {
        m.childs.forEach((c) => {
          c.my_message = c.person_id === req.session.person.id;
          // delete c.user_id;
        });
      }
    });
    res.send(messages);
  } catch (e) {
    console.log(e.message);
    res.status(400).send('Сообщения не были получены [Bad request]');
  }
  return 'Ok';
});

app.post('/api/get_message_comments', validate_user, async (req, res) => {
  if (!availableParam(req.body, 'parent_message_id')
      || isEmptyString.test(req.body.parent_message_id)) {
    return res.status(400).send('Не корректный запрос [Bad request]');
  }

  try {
    const messages = (await db.query(`
      SELECT (SELECT jsonb_build_object(
                       'avatar_link', tu.avatar_link,
                       'person_fio_short', system.make_fio_short(t1.person_lastname, t1.person_name, t1.person_fathername))
              FROM staff.tbluser tu
                     JOIN staff.tblperson t1 ON tu.id = t1.user_id
              WHERE t1.id = m.person_id) AS person,
             m.id,
             m.parent_id,
             m.person_id,
             m.content,
             m.post_stamp,
             m.change_stamp,
             m.attached,
             (SELECT COUNT(id)
              FROM message
              WHERE m.id = message.parent_id
                AND NOT message.deleted) AS child_count
      FROM message m
      WHERE NOT m.deleted
        AND m.parent_id = $1
      ORDER BY m.post_stamp`,
    [req.body.parent_message_id])).rows;

    messages.forEach((m) => {
      m.my_message = m.user_id === req.session.person.user_id;
      // delete m.user_id;
    });

    res.send(messages);
  } catch (e) {
    console.log(e.message);
    res.status(400).send('Сообщения не были получены [Bad request]');
  }

  return 'Ok';
});

app.post('/api/get_message_search_like', validate_user, async (req, res) => {
  if (!availableParam(req.body, 'search_text', 'page')) {
    return res.status(400).send('Не корректный запрос [Bad request]');
  }

  try {
    // с клиента может прийти запрос с масками
    const escape_wildcards = req.body.search_text.replace(/([%_])/gm, '\\$1');
    const search_text = `%${escape_wildcards}%`;
    const offset = req.body.page * PAGE_LIMIT_MESSAGE;
    const current_person_id = req.session.person.id;

    const messages = (await db.query(`
      SELECT (
               SELECT jsonb_build_object(
                        'avatar_link', tu.avatar_link,
                        'person_fio_short', system.make_fio_short(t1.person_lastname, t1.person_name, t1.person_fathername))
               FROM staff.tbluser tu
                      JOIN staff.tblperson t1 ON tu.id = t1.user_id
               WHERE t1.id = m.person_id) AS person,
             m.id,
             m.person_id,
             m.parent_id,
             m.content,
             m.post_stamp,
             m.attached,
             m.shared,
             m.plain_content
      FROM message m
             JOIN staff.view_person_lite_simple vp ON m.person_id = vp.id
      WHERE NOT m.deleted
        AND (m.person_id = $4
        OR EXISTS(
               SELECT *
               FROM shared_message sm1
               WHERE sm1.message_id = m.id
                 AND sm1.person_id = $4)
        OR (NOT EXISTS(SELECT * FROM shared_message sm WHERE sm.message_id = m.id)))
        AND ((m.plain_content ILIKE $1
        OR ((m.attached IS NOT NULL) AND
            EXISTS(SELECT * FROM jsonb_array_elements(m.attached) AS a WHERE a ->> 'name' ILIKE $1))))
      ORDER BY m.post_stamp DESC
      LIMIT $2 OFFSET $3`,
    [
      search_text,
      PAGE_LIMIT_MESSAGE,
      offset,
      current_person_id,
    ])).rows;

    messages.forEach((m) => {
      m.my_message = m.person.id === req.session.person.user_id;
      // delete m.user_id;
    });

    res.send(messages);
  } catch (e) {
    console.log(e.message);
    res.status(400).send('Сообщения не были получены [Bad request]');
  }

  return 'Ok';
});

app.post('/api/get_search_participant', validate_user, async (req, res) => {
  if (!availableParam(req.body, 'search_text') || req.body.search_text.length < 3) {
    return res.status(400).send('Не корректный запрос [Bad request]');
  }

  try {
    // с клиента может прийти запрос с масками
    const escape_wildcards = req.body.search_text.replace(/([%_])/gm, '\\$1');
    const search_text = `%${escape_wildcards}%`;
    const current_person_id = req.session.person.id;
    const persons = (await db.query(`
      SELECT vp.id, vp.person_fio_full, vp.person_fio_short, vp.avatar_link
      FROM staff.view_person_lite_simple vp
      WHERE vp.id != $1
       AND vp.person_fio_full ILIKE $2`,
    [
      current_person_id,
      search_text,
    ])).rows;

    res.send(persons);
  } catch (e) {
    console.log(e);
    res.status(400).send('Участники не были получены [Bad request]');
  }

  return 'Ok';
});

app.post('/api/get_shared_participant', validate_user, async (req, res) => {
  if (!availableParam(req.body, 'id')) {
    return res.status(400).send('Не корректный запрос [Bad request]');
  }

  try {
    const message_id = req.body.id;
    const current_person_id = req.session.person.id;
    const persons = (await db.query(`
      WITH shared_on_message AS (
             SELECT person_id, marked_state, marked_date
             FROM shared_message
             WHERE message_id = $1
           ),
           persons AS (
             SELECT tu.avatar_link,
                    system.make_fio_full(p.person_lastname, p.person_name, p.person_fathername) AS person_fio_full,
                    s.marked_state,
                    s.marked_date
             FROM shared_on_message s
                    JOIN staff.tblperson p ON s.person_id = p.id
                    JOIN staff.tbluser tu ON tu.id = p.user_id
           )
      SELECT avatar_link, person_fio_full, marked_state, marked_date
      FROM persons
      WHERE EXISTS(SELECT *
                   FROM message
                   WHERE id = $1
                     AND person_id = $2)
         OR $2 IN (SELECT person_id FROM shared_on_message)`,
    [
      message_id,
      current_person_id,
    ])).rows;

    res.send(persons);
  } catch (e) {
    console.log(e);
    res.status(400).send('Участники не были получены [Bad request]');
  }
});

app.get('/auth', async (req, res) => {
  if (req.session.person && (await get_user_async(req.session.person.user_id))) {
    res.redirect('/');
  } else {
    res.sendFile(path.join(FOLDER_VIEWS, 'auth.html'));
  }
});

app.post('/auth', async (req, res) => {
  if (!availableParam(req.body, 'user_name', 'user_pwd')
      || isEmptyString.test(req.body.user_name)
      || isEmptyString.test(req.body.user_pwd)) {
    return res.status(400).send('Не корректный запрос [Bad request]');
  }
  try {
    const user = (await db.query(`
      SELECT id, user_name
      FROM staff.view_person_lite_simple
      WHERE user_name = $1 AND user_pwd = $2`,
    [req.body.user_name, req.body.user_pwd])).rows[0];

    if (user && availableParam(user, 'id', 'user_name')) {
      req.session.person = {
        id: user.id,
        user_name: user.user_name,
      };
      res.redirect('/');
    } else {
      res.status(401).send('Аутентификация не пройдена');
    }
  } catch (e) {
    console.log(e.message);
    res.status(401).send('Аутентификация не пройдена');
  }

  return 'Ok';
});

app.post('/api/logout', validate_user, (req, res) => {
  req.session.destroy();
  res.status(200).send('Ok');
});

app.get('/avatar/:name', validate_user, (req, res) => {
  if (!availableParam(req.params, 'name')) {
    return res.status(400).send('Не корректный запрос [Bad request]');
  }

  const file = path.join(FOLDER_AVATARS, req.params.name);
  fs
    .promises
    .access(file, fs.constants.R_OK)
    .then(() => fs.createReadStream(file).pipe(res))
    .catch(() => res.status(404).send('Файл не найден [File not found]'));

  return 'Ok';
});

app.get('/', validate_user, (req, res) => {
  res.sendFile(path.join(FOLDER_VIEWS, 'index.html'));
});

app.use((req, res) => {
  res.status(404).send('Ресурс не найден [File not found]');
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
