const del = require('del');
const gulpif = require('gulp-if');
const babel = require('gulp-babel');
const rename = require('gulp-rename');
const concat = require('gulp-concat');
const htmlmin = require('gulp-htmlmin');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify-es').default;
const {
  src, dest, parallel, series,
} = require('gulp');

let minify = false;
let polyfill = false;
const dir = './dist';

const babelOptions = {
  presets: [
    [
      '@babel/preset-env',
      {
        targets: {
          chrome: '49',
          ie: '11',
        },
      },
    ],
  ],
  plugins: ['array-includes'],
  overrides: [{
    sourceType: 'script',
  }],
  compact: true,
  comments: false,
};
const cssOptions_1 = {
  level: {
    1: {
      all: false,
      specialComments: false,
    },
  },
};
const cssOptions_2 = {
  level: {
    2: {
      removeDuplicateRules: true,
    },
  },
  compatibility: {
    properties: {
      colors: false,
    },
  },
};
const htmlOptions = {
  collapseWhitespace: true,
  removeComments: true,
};

function style_bootstrap() {
  return src('./src/css/bootstrap.min.css')
    .pipe(gulpif(minify, cleanCSS(cssOptions_1)))
    .pipe(gulpif(minify, cleanCSS(cssOptions_2)))
    .pipe(rename('bs.css'))
    .pipe(dest(`${dir}/css`));
}

function style() {
  return src([
    './src/css/filepond.min.css',
    './src/css/filepond-plugin-get-file.min.css',
    './src/css/trumbowyg.min.css',
    './src/css/trumbowyg-custom.css',
    './src/css/trumbowyg.colors.min.css',
    './src/css/font-awesome.min.css',
    './src/css/sweetalert2.min.css',
    './src/css/main.css',
    './src/css/chip-shared.css',
    './src/css/comment-tree.css',
    './src/css/list-message.css',
    './src/css/list-shared.css',
    './src/css/message-content-collapsable.css',
  ])
    .pipe(gulpif(minify, cleanCSS(cssOptions_1)))
    .pipe(gulpif(minify, cleanCSS(cssOptions_2)))
    .pipe(concat('app.min.css'))
    .pipe(dest(`${dir}/css`));
}

function asset() {
  return src('./src/asset/*')
    .pipe(dest(`${dir}/asset`));
}

function html() {
  return src('./src/*.html')
    .pipe(gulpif(minify, htmlmin(htmlOptions)))
    .pipe(dest(`${dir}/`));
}

function app() {
  return src('./src/js/app.js')
    .pipe(gulpif(polyfill, babel(babelOptions)))
    .pipe(gulpif(minify, uglify()))
    .pipe(dest(`${dir}/js`));
}

function app_bundle() {
  return src([
    './src/js/plugins/jquery-3.5.1.min.js',
    './src/js/plugins/jquery.color-2.2.0.min.js',
    './src/js/plugins/vue.min.js',
    './src/js/plugins/bootstrap.min.js',
    './src/js/plugins/moment.min.js',
    './src/js/plugins/moment.ru.min.js',
    './src/js/plugins/vue-infinite-loading.js',
    './src/js/plugins/filepond.min.js',
    './src/js/plugins/vue-filepond.min.js',
    './src/js/plugins/filepond-plugin-get-file.min.js',
    './src/js/plugins/filepond-plugin-file-validate-size.min.js',
    './src/js/plugins/trumbowyg.min.js',
    './src/js/plugins/trumbowyg.ru.min.js',
    './src/js/plugins/trumbowyg.fontsize.min.js',
    './src/js/plugins/trumbowyg.colors.min.js',
    './src/js/plugins/trumbowyg.cleanpaste.min.js',
    './src/js/plugins/trumbowyg.noembed.min.js',
    './src/js/plugins/trumbowyg.upload.min.js',
    './src/js/plugins/trumbowyg.unpasteimage.js',
    './src/js/plugins/vue-trumbowyg.min.js',
    './src/js/plugins/socket.io.slim.js',
    './src/js/plugins/sweetalert2.min.js',
    './src/js/plugins/jquery.mark.es6.min.js',
  ])
    .pipe(gulpif(polyfill, babel(babelOptions)))
    .pipe(gulpif(minify, uglify()))
    .pipe(concat('app.bundle.js'))
    .pipe(dest(`${dir}/js`));
}

function main_pack() {
  return src([
    './src/js/util.js',
    './src/js/comment-tree.js',
    './src/js/message-content-collapsable.js',
    './src/js/message-content-search.js',
    './src/js/list-shared-item.js',
  ])
    .pipe(gulpif(polyfill, babel(babelOptions)))
    .pipe(gulpif(minify, uglify()))
    .pipe(dest(`${dir}/js`));
}

function fonts() {
  return src('./src/fonts/*')
    .pipe(dest(`${dir}/fonts`));
}

function clean() {
  return del(`${dir}/`);
}

async function min() {
  minify = true;
}

async function poly() {
  minify = true;
  polyfill = true;
}

// full distribute
exports.default = series(
  clean,
  parallel(
    app,
    html,
    asset,
    style,
    app_bundle,
    main_pack,
    style_bootstrap,
    fonts,
  ),
);

exports.min = series(
  clean,
  min,
  parallel(
    app,
    html,
    asset,
    style,
    app_bundle,
    main_pack,
    style_bootstrap,
    fonts,
  ),
);

exports.poly = series(
  clean,
  poly,
  parallel(
    app,
    html,
    asset,
    style,
    app_bundle,
    main_pack,
    style_bootstrap,
    fonts,
  ),
);
