/* global $, util, Vue, eventBus */

(function () {
  const unit_rem = parseFloat($('body').css('font-size'));
  const collapsed_size_rem = 20;

  Vue.component('message-content-collapsable', {
    template: `
  <div class="w-100" ref="content">
    <div :id="\`message-content-collapsable-\${dataid}\`" class="msg-text col-12" v-html="data"></div>

    <div v-if="collapsable" :class="['col-12', {'contentcut': collapsed}]">
      <span @click="fade($event)" class="link">
        <span v-if="collapsed">
          Показать полностью <i class="fa fa-chevron-down" aria-hidden="true"></i>
        </span>
        <span v-if="!collapsed">
          Свернуть <i class="fa fa-chevron-up" aria-hidden="true"></i>
        </span>
       </span>
    </div>
  </div>
  `,
    props: {
      data: Object,
      dataid: Object,
    },
    data() {
      return {
        collapsable: false,
        collapsed: true,
        total_height_rem: null,
        $element: null,
        $message_div: null,
        width: null,
        height: null,
      };
    },
    mounted() {
      this.$element = $(`div#message-content-collapsable-${this.dataid}`);
      this.$message_div = $(`div[data-key="${this.dataid}"]`);

      const component_this = this;
      $(this.$element).find('img').each(function () {
        const $this = $(this);
        const src = $this.attr('src');
        if (util.isEmptyString(src)) return;
        if (!$this.attr('alt') || util.isEmptyString($this.attr('alt'))) {
          $this.attr('alt', src.split('/').pop());
        }
        $this.wrap(`<a href="${src}" target="_blank"></a>`);
        $this.on('load', () => component_this.resizeWindow());
      });
      this.$nextTick(() => {
        this.total_height_rem = this.$element.css('height', 'auto').height() / unit_rem;
        this.collapsable = this.total_height_rem >= collapsed_size_rem;
        this.resizeWindow();
      });
      eventBus.$on('onresize', () => {
        const width = this.$element.width();
        if (width === this.width) return;
        this.width = width;
        this.resizeWindow();
      });
    },
    watch: {
      data() {
        this.$nextTick(() => {
          this.resizeWindow();
        });
      },
    },
    methods: {
      resizeWindow() {
        this.total_height_rem = this.$element.css('height', 'auto').height() / unit_rem;

        this.collapsable = this.total_height_rem >= collapsed_size_rem;
        if (this.collapsable && this.collapsed) {
          // изменение высоты только если контент сворачиваем и свёрнут
          this.$element.height(collapsed_size_rem * unit_rem);
        }
      },
      fade(e) {
        e.preventDefault();
        if (util.isOverHalfViewport(this.$element)) {
          util.scrollInto(this.$message_div[0], 'start', 'auto');
        }

        if (this.collapsable && this.collapsed) {
          const current_height = this.$element.height();
          const total_height = this.$element.css('height', 'auto').height();
          this.$element
            .css({ height: current_height })
            .animate({
              height: total_height,
            },
            {
              duration: 200,
              complete: () => {
                this.$element.css('height', 'auto');
                this.collapsed = false;
              },
            });
        } else if (this.collapsable && !this.collapsed) {
          this.collapsed = true;
          const total_height = this.$element.css('height', 'auto').height();
          const collapsed_height = collapsed_size_rem * unit_rem;
          this.$element
            .css({ height: total_height })
            .animate({ height: collapsed_height },
              {
                duration: 200,
                complete: () => {
                  this.$element.height(collapsed_height);
                },
              });
        }
      },
    },
  });
}());
