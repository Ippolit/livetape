/* global $, Vue */

(function () {
  Vue.component('message-content-searchable', {
    template: `
    <div :id="\`message-content-searchable-\${dataid}\`" class="searchable">
      <div v-html="data"></div>
    </div>
    `,
    props: {
      data: Object,
      dataid: Object,
      search_text: Object,
    },
    mounted() {
      const $element = $(`div#message-content-searchable-${this.dataid}`);
      $element.mark(this.search_text, {
        element: 'span',
        accuracy: 'partially',
        className: 'search-mark',
        ignoreJoiners: true,
      });
    },
  });
}());
