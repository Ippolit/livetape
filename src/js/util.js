/* global $ */

const util = {
  isEmptyString(str) {
    return /^\s*$/i.test(str);
  },
  isArray(obj) {
    return Array.isArray(obj);
  },
  isFilledArray(array) {
    return this.isArray(array) && array.length !== 0;
  },
  isFunc(functionToCheck) {
    return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
  },
  availableParams(obj, ...params) {
    return obj && this.isFilledArray(params) && params.every((p) => p in obj);
  },
  clearFilePond(filepond) {
    const attached = filepond.getFiles();
    if (util.isFilledArray(attached)) {
      attached.forEach((f) => filepond.removeFiles(f));
    }
  },
  findMessageById(msgs, id) {
    // eslint-disable-next-line no-restricted-syntax
    for (const m of msgs) {
      if (m.id === id) {
        return m;
      } if (util.isFilledArray(m.childs)) {
        const message = this.findMessageById(m.childs, id);
        if (message) return message;
      }
    }
    return null;
  },
  findMessageInTreeById(tree, id) {
    // eslint-disable-next-line no-restricted-syntax
    for (const t of tree) {
      if (id in t.$refs) {
        return t.$refs[id];
      }
      if ('comment-tree' in t.$refs) {
        const res = this.findMessageInTreeById(t.$refs['comment-tree'], id);
        if (res) return res;
      }
    }
    return null;
  },
  scrollInto(el, block, behavior = 'smooth') {
    el.scrollIntoView({
      block,
      behavior,
    });
  },
  jqAnimateBlink(el, valueStart, valueEnd, duration) {
    if (el && valueStart && valueEnd) {
      el
        .css('background-color', valueStart)
        .animate(
          {
            'background-color': valueEnd,
          },
          {
            easing: 'linear',
            duration,
            complete: () => {
              el
                .css('background-color', valueEnd)
                .animate({
                  'background-color': valueStart,
                },
                {
                  duration,
                  easing: 'linear',
                });
            },
          },
        );
    }
  },
  isOverHalfViewport($this) {
    const elementTop = $this.offset().top;
    const elementBottom = elementTop + $this.outerHeight();

    const viewportTop = $(window).scrollTop();

    return elementTop < (viewportTop - $this.outerHeight() / 2)
        && elementBottom > viewportTop;
  },
  matchAll(pattern, text) {
    const matches = [];
    let match;
    if (pattern instanceof RegExp) {
      // eslint-disable-next-line no-cond-assign
      while ((match = pattern.exec(text)) !== null) {
        matches.push(match[1]);
      }
    }
    return matches;
  },
  declensionOfNumbers(number, ...words) {
    const n = number % 100;
    const n1 = n % 10;
    if (n > 10 && n < 20) { return words[2]; } // Родительный множ
    if (n1 > 1 && n1 < 5) { return words[1]; } // Родительный
    if (n1 === 1) { return words[0]; } // Именительный
    return words[2];
  },
};
