/* global Swal, $, util, Vue, eventBus */

Vue.component('comment-tree', {
  template: '#comment-tree-template',
  data() {
    return {
      isMobile: false,
      editor_config: {
        tagsToRemove: ['script'],
        svgPath: '/asset/trumbowyg.icons.svg',
        btnsDef: {
          image: {
            dropdown: [
              'insertImage',
              'upload',
              'noembed',
            ],
            ico: 'insertImage',
            title: 'Изображение',
          },
        },
        plugins: {
          upload: {
            serverPath: '/api/send_inner_attach',
            fileFieldName: 'uf',
          },
        },
        btns: [
          ['viewHTML'],
          ['formatting'],
          ['fontsize'],
          ['foreColor', 'backColor'],
          ['strong', 'em', 'del'],
          ['superscript', 'subscript'],
          ['link'],
          ['image'],
          [
            'justifyLeft',
            'justifyCenter',
            'justifyRight',
            'justifyFull',
          ],
          ['unorderedList', 'orderedList'],
          ['horizontalRule'],
          ['removeformat'],
          ['fullscreen'],
        ],
        lang: 'ru',
      },
    };
  },
  props: {
    comments: Object,
  },
  created() {
    this.isMobile = window.innerWidth < 768;
    eventBus.$on('onresize', this.resizeWindow);
  },
  methods: {
    onMarkViewMessage(message) {
      $.ajax({
        type: 'POST',
        url: '/api/mark_view_message',
        data: { id: message.id },
        success: (result) => {
          message.marked_state = result;
        },
        error(e) {
          console.log(e);
        },
      });
    },
    sharedParticipantModalShow(message) {
      this.$emit('shared-participant-modal-show', message);
    },
    resizeWindow() {
      this.isMobile = window.innerWidth < 768;
    },
    getComments(msg_id, e) {
      // отправка события на получение комментариев вверх, для доступа к родительскому сообщению
      this.$emit('get-comments', msg_id);
    },
    onDeleteCommentRequest(comment, e) {
      const plain_content = util.matchAll(/<[^>]+>([^<>]+)/gim, comment.content).join(' ');

      Swal.fire({
        title: 'Удалить сообщение?',
        html: plain_content.length <= 100 ? plain_content : `${plain_content.slice(0, 100)}...`,
        icon: 'question',
        showCancelButton: true,
        confirmButtonText: 'Да, удалить',
        cancelButtonText: 'Отмена',
        confirmButtonColor: 'hsl(211, 100%, 50%)',
        cancelButtonColor: 'hsl(354, 70%, 50%)',
        focusCancel: true,
      })
        .then((result) => {
          if (result.isConfirmed) {
            $.ajax({
              type: 'POST',
              url: '/api/message_delete',
              data: {
                id: comment.id,
              },
              error(error) {
                console.log(error);
              },
            });
          }
        });
    },
    onAppendCommentMode(comment, e) {
      // режим ответа на сообщение
      comment.child_append_mode = true;
      comment.child_editing_mode = false;
      comment.child_editor_content = '';
      comment.child_editor_filepond.splice(0);
    },
    onSendComment(comment) {
      // отправка ответа
      const m = {
        content: comment.child_editor_content,
        parent_id: comment.id,
        attached: [],
      };
      if (util.isFilledArray(comment.child_editor_filepond)) {
        m.attached.push(...comment.child_editor_filepond.map((f) => ({
          id: f.id,
          name: f.filename,
        })));
      }
      const filepond = this.$refs[`pond-comment-${comment.id}`][0];

      // завершение режима ответа
      comment.child_append_mode = false;
      comment.child_editing_mode = false;
      comment.child_editor_content = '';
      util.clearFilePond(filepond);

      $.ajax({
        type: 'POST',
        url: '/api/send_comment',
        data: m,
        success() {
        },
        error(e) {
          console.log(e);
        },
      });
    },
    onEditingMode(comment, e) {
      // режим изменения своего сообщения
      comment.child_editing_mode = true;
      comment.child_append_mode = false;
      comment.child_editor_content = comment.content;
      comment.child_editor_attached.splice(0);
      comment.child_editor_filepond.splice(0);

      if (util.isFilledArray(comment.attached)) {
        // копирование файлов через spread чтобы избавиться от реактивности
        comment.child_editor_attached = comment.attached.map((f) => ({ ...f }));
      }
      this.$nextTick(() => {
        // при уже установленном фокусе на ином редакторе необходимо установить на текущий
        $(`textarea[data-ref="editor_comment-${comment.id}"]`)
          .closest('.trumbowyg-box')
          .find('.trumbowyg-editor')
          .focus();
      });
    },
    onCancelEditing(comment) {
      // выход как из режима редактирования, так и ответа
      comment.child_editing_mode = false;
      comment.child_append_mode = false;
      comment.child_editor_content = '';
    },
    onApplyEditing(comment) {
      // изменение сообщения
      const change_comment = {
        id: comment.id,
        parent_id: comment.child_parent_id,
        content: comment.child_editor_content,
        attached: comment.child_editor_attached,
      };

      if (util.isFilledArray(comment.child_editor_filepond)) {
        // добавление новых загруженных файлов
        change_comment.attached.push(...comment.child_editor_filepond.map((f) => ({
          id: f.id,
          name: f.filename,
        })));
      }
      const filepond = this.$refs[`pond-comment-${comment.id}`][0];
      $.ajax({
        type: 'POST',
        url: '/api/change_message',
        data: change_comment,
        success() {
          // завершение режима редактирования
          comment.child_append_mode = false;
          comment.child_editing_mode = false;
          comment.child_editor_content = '';
          util.clearFilePond(filepond);
        },
      });
    },
    removeFileAttachOnEditingMode(comment, file) {
      // удаление присутствующего файла в режиме редактирования
      const file_index = comment.child_editor_attached.findIndex((f) => f.id === file.id);
      if (file_index !== -1) {
        comment.child_editor_attached.splice(file_index, 1);
      }
    },
    removeFilePondOnEditingMode(comment, file) {
      // удаление только что добавленного файла в режиме редактирования
      const filepond = this.$refs[`pond-comment-${comment.id}`][0];
      const file_index = comment.child_editor_filepond.findIndex((f) => f.id === file.id);
      const filepond_file = filepond.getFiles().find((f) => f.serverId === file.id);
      if (file_index !== -1 && filepond_file) {
        comment.child_editor_filepond.splice(file_index, 1);
        filepond.removeFile(filepond_file.id);
      }
    },
    handleFilePondInit(comment) {
      // привязка filepond к сообщению
      const filepond = this.$refs[`pond-comment-${comment.id}`][0];

      filepond.server = {
        process: (fieldName, file, metadata, load, error, progress, abort, transfer, options) => {
          // обработка отправки файла

          const formData = new FormData();
          formData.append(fieldName, file, file.name);
          const xhr = $.ajaxSettings.xhr();
          xhr.upload.onprogress = (e) => {
            progress(e.lengthComputable, e.loaded, e.total);
          };
          xhr.addEventListener('load', () => {
            // если ответ успешный - добавление информации о файле
            // и добавление его в массив только что добавленных

            if (xhr.status >= 200 && xhr.status < 300) {
              file.filename = file.name;
              file.id = xhr.responseText;

              comment.child_editor_filepond.push(file);
              load(xhr.responseText);
            } else {
              error();
            }
          }, false);

          $.ajax({
            async: true,
            cache: false,
            type: 'POST',
            url: '/api/send_attach',
            data: formData,
            processData: false,
            contentType: false,
            xhr: () => xhr,
          });
          return {
            // отмена загрузки файла во время отправки
            abort: () => {
              xhr.abort();
              abort();
            },
          };
        },
        revert: (uniqueFileId, load, error) => {
          // обработка удаления файла

          const index = comment.child_editor_filepond.findIndex((f) => f.id === uniqueFileId);
          if (index !== -1) {
            $.ajax({
              type: 'DELETE',
              url: '/api/send_attach',
              data: uniqueFileId,
              success() {
                comment.child_editor_filepond.splice(index, 1);
                load();
              },
            });
          }
        },
        fetch: null,
        load: null,
      };
    },
  },
});
