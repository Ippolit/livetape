/* global
io,
vueFilePond,
FilePondPluginGetFile,
FilePondPluginFileValidateSize,
Vue,
$,
moment,
VueTrumbowyg,
util */

const socket = io();
const FilePond = vueFilePond.default(FilePondPluginGetFile, FilePondPluginFileValidateSize);
const Trumbowyg = VueTrumbowyg.default;

Vue.use(Trumbowyg);

// массив экранов в соответствии с bs4 для разделения уровня иерархии
const ratioSizes = [
  {
    availWidth: 576,
    modulo: 3,
    class: ['comment_next-floor', 'comment_next-floor-1'],
  },
  {
    availWidth: 768,
    modulo: 7,
    class: ['comment_next-floor', 'comment_next-floor-2'],
  },
  {
    availWidth: 992,
    modulo: 10,
    class: ['comment_next-floor', 'comment_next-floor-3'],
  },
  {
    availWidth: 1200,
    modulo: 14,
    class: ['comment_next-floor', 'comment_next-floor-4'],
  },
];

function getRatioSize() {
  return ratioSizes.find((x) => x.availWidth >= window.innerWidth) || ratioSizes[3];
}

function applyCommentClass(msg, ratio) {
  if (msg.depth_indent && msg.depth_indent !== 0) {
    msg.child_classes.splice(1);
    if (msg.depth_indent % ratio.modulo === 0) {
      msg.child_classes.push(...ratio.class);
    }
  }
}

function setCommentClasses(msgs, ratio) {
  // eslint-disable-next-line no-restricted-syntax
  for (const m of msgs) {
    applyCommentClass(m, ratio);
    if (util.isFilledArray(m.childs)) {
      setCommentClasses(m.childs, ratio);
    }
  }
}

moment.locale('ru');
moment.updateLocale('ru', {
  relativeTime: {
    past(number) {
      return number === 'только что' ? number : `${number} назад`;
    },
    s: 'только что',
    ss: 'только что',
  },
});

const eventBus = new Vue();
window.addEventListener('resize', () => {
  eventBus.$emit('onresize');
});

const app = new Vue({
  el: '#live',
  data: {
    shared_participant_view: [],
    search_participant_text: '',
    shared_participant_select: [],
    shared_participant_buffer: [],
    shared_participant_finded: [],
    search_text: '',
    search_mode: false,
    search_messages: [],
    search_page: 0,
    search_infinity_id: +new Date(),
    search_infinity_show: false,
    ratio: getRatioSize(),
    username: null,
    user_id: null,
    messages: [],
    page: 0,
    editor_content: '',
    control_reading_post: false,
    editor_config: {
      tagsToRemove: ['script'],
      svgPath: '/asset/trumbowyg.icons.svg',
      btnsDef: {
        image: {
          dropdown: [
            'insertImage',
            'upload',
            'noembed',
          ],
          ico: 'insertImage',
          title: 'Изображение',
        },
      },
      plugins: {
        upload: {
          serverPath: '/api/send_inner_attach',
          fileFieldName: 'uf',
        },
      },
      btns: [
        ['viewHTML'],
        ['formatting'],
        ['fontsize'],
        ['foreColor', 'backColor'],
        ['strong', 'em', 'del'],
        ['superscript', 'subscript'],
        ['link'],
        ['image'],
        [
          'justifyLeft',
          'justifyCenter',
          'justifyRight',
          'justifyFull',
        ],
        ['unorderedList', 'orderedList'],
        ['horizontalRule'],
        ['removeformat'],
        ['fullscreen'],
      ],
      lang: 'ru',
    },
    editing_files_pond: [],
    request_search_participant_cancellation: null,
    request_search_is_aborted: false,
  },
  components: {
    FilePond,
  },
  watch: {
    search_text: (newVal, oldVal) => {
      if (oldVal.length > 0 && newVal.length === 0) {
        app.search_mode = false;
        app.search_messages.splice(0);
      }
    },
    ratio: (newRatio) => {
      setCommentClasses(app.messages, newRatio);
    },
  },
  methods: {
    sharedParticipantShow(message) {
      app.shared_participant_view.splice(0);
      const data = {
        id: message.id,
      };
      $.ajax({
        type: 'POST',
        url: '/api/get_shared_participant',
        data,
        success: (participants) => {
          if (util.isFilledArray(participants)) {
            app.shared_participant_view = participants
              .map((p) => ({
                avatar_link: `/avatar/${p.avatar_link || 'no_avatar_30.png'}`,
                person_fio_full: p.person_fio_full,
                marked_date: p.marked_date ? moment(p.marked_date).format('DD.MM.YYYY') : null,
                marked_date_tooltip: p.marked_date ? moment(p.marked_date).format('DD.MM.YYYY HH:mm:ss') : null,
              }));
          }
        },
        error(e) {
          console.log(e);
        },
      });
      $('#view_shared').modal('show');
    },
    search_participant(eventInput) {
      app.search_participant_text = eventInput.target.value;

      let i = app.shared_participant_finded.length;
      // eslint-disable-next-line no-plusplus
      while (i--) {
        if (!app.shared_participant_finded[i].isSelect) {
          app.shared_participant_finded.splice(i, 1);
        }
      }

      app.shared_participant_finded.forEach((p) => {
        p.search_text = app.search_participant_text;
      });
      app.shared_participant_buffer.forEach((p) => {
        p.search_text = app.search_participant_text;
      });

      if (app.search_participant_text.length < 3) {
        return;
      }
      if (app.request_search_participant_cancellation) {
        app.request_search_is_aborted = true;
        app.request_search_participant_cancellation.abort();
      }

      const data = {
        search_text: app.search_participant_text,
      };

      app.request_search_participant_cancellation = $.ajax({
        type: 'POST',
        url: '/api/get_search_participant',
        data,
        beforeSend: () => {
          app.request_search_is_aborted = false;
        },
        success: (participants) => {
          if (util.isFilledArray(participants)) {
            participants = participants
              .filter((p) => !app.shared_participant_buffer.some((spb) => p.id === spb.id)
                && !app.shared_participant_finded.some((spf) => p.id === spf.id));

            participants.forEach((p) => {
              p.isSelect = false;
              p.search_text = app.search_participant_text;
              p.avatar_link = `/avatar/${p.avatar_link || 'no_avatar_30.png'}`;
            });
            if (!app.request_search_is_aborted) {
              app.shared_participant_finded.push(...participants);
            }
          }
          app.request_search_participant_cancellation = null;
          app.request_search_is_aborted = false;
        },
        error(e) {
          console.log(e);
          app.request_search_participant_cancellation = null;
        },
      });
    },
    participant_select_change(person) {
      // выбранная персона не требует обработки
      if (person.isSelect) return;
      const idx_f = app.shared_participant_finded.findIndex((p) => p.id === person.id);
      const idx_b = app.shared_participant_buffer.findIndex((p) => p.id === person.id);

      if (idx_f >= 0) {
        // если с персоны снят выбор, и фио не подходит под поисковый запрос - удаление
        if (!(new RegExp(`${app.search_participant_text}`, 'gim').test(person.person_fio_full))
          || app.search_participant_text.length < 3) {
          app.shared_participant_finded.splice(idx_f, 1);
        }
      } if (idx_b >= 0) {
        const condition_fio = new RegExp(`${app.search_participant_text}`, 'gim').test(person.person_fio_full);
        if (!condition_fio || app.search_participant_text.length < 3) {
          // если с ранее выбранной персоны снят выбор, и фио не подходит под поисковый запрос - удаление
          app.shared_participant_buffer.splice(idx_b, 1);
        } else if (condition_fio) {
          // если фио подходит - перемещение в finded
          app.shared_participant_finded.unshift({ ...person });
          app.shared_participant_buffer.splice(idx_b, 1);
        }
      }
    },
    on_shared_participant_modal_close() {
      app.search_participant_text = '';
      app.shared_participant_finded.splice(0);
      $('#choose_shared').modal('hide');
    },
    on_shared_participant_modal_apply() {
      const selected = app
        .shared_participant_finded
        .filter((p) => p.isSelect)
        // освобождение от реактивности и очистка предыдущего поиска
        .map((p) => ({ ...p, search_text: '' }));
      app.shared_participant_select.splice(0, app.shared_participant_select.length, ...selected, ...app.shared_participant_buffer);
      app.search_participant_text = '';
      app.shared_participant_finded.splice(0);
      $('#choose_shared').modal('hide');
    },
    on_shared_participant_modal_show() {
      const shared = app.shared_participant_select.map((p) => ({ ...p }));
      app.shared_participant_buffer.splice(0, app.shared_participant_buffer.length, ...shared);
      $('#choose_shared').modal('show');
    },
    search() {
      // не давать инициировать самостоятельную загрузку первой страницы
      // причина - не стабильная работа infinity-loader
      app.search_infinity_show = false;
      app.search_page = 0;
      app.search_messages.splice(0);
      app.search_mode = true;
      // вынужденное дублирование первоначальной загрузки
      // т.к. infinity-loader не срабатывает при повторном поиске -
      // изменение состояния identifier не даёт положительного результата
      const data = {
        page: app.search_page,
        search_text: app.search_text,
      };
      $.ajax({
        type: 'POST',
        url: '/api/get_message_search_like',
        data,
        success: (messages) => {
          if (util.isFilledArray(messages)) {
            const msgs = messages.map((m) => this.processSearchMessage(m, data.search_text));
            app.search_messages.push(...msgs);
            app.search_page = 1;
            app.search_infinity_show = true;
          } else {
            app.search_infinity_show = true;
            app.$refs.infinity_search.status = 2; // no-results: статус отсутствия сообщений
          }
        },
        error(e) {
          console.log(e);
          app.search_infinity_show = true;
          app.$refs.infinity_search.status = 3; // error: статус ошибки
        },
      });
    },
    processSearchMessage(msg, search_text) {
      const m = {
        person_fio_short: msg.person.person_fio_short,
        avatar_link: `/avatar/${msg.person.avatar_link || 'no_avatar_30.png'}`,
        id: msg.id,
        attached: msg.attached || [],
        classes: ['message_post', 'list-message-item'],
        content: msg.content,
        finded: search_text,
        child_parent_id: msg.parent_id || null,
        post_stamp: moment(msg.post_stamp).fromNow(),
        change_stamp: msg.change_stamp ? moment(msg.change_stamp).fromNow() : null,
        post_stamp_tooltip: moment(msg.post_stamp).format('DD.MM.YYYY HH:mm:ss'),
        change_stamp_tooltip: msg.change_stamp ? moment(msg.change_stamp).format('DD.MM.YYYY HH:mm:ss') : null,
      };

      if (util.isFilledArray(m.attached)) {
        m.attached.forEach((f) => {
          f.link = `/api/get_attach/${m.id}/${f.id}`;
          f.class = new RegExp(`${search_text}`, 'gim').test(f.name)
            ? 'file_attached badge badge-pill badge-warning'
            : 'file_attached badge badge-pill badge-primary';
        });
      }

      if (m.child_parent_id) {
        m.classes.push('search-message-child');
      }

      return m;
    },
    sendMessage() {
      const msg = {
        content: app.editor_content,
        shared: app.shared_participant_select.map((x) => ({ id: x.id })),
        control_reading_post: app.control_reading_post,
      };
      const files = app.$refs.filepond.getFiles();

      if (util.isFilledArray(files)) {
        msg.attached = files.map((x) => ({
          id: x.serverId,
          name: x.filename,
        }));
      }

      $.ajax({
        type: 'POST',
        url: '/api/send_message',
        data: msg,
        success: () => {
          app.control_reading_post = false;
          app.editor_content = '';
          util.clearFilePond(app.$refs.filepond);
          app.shared_participant_select.splice(0);
        },
        error(e) {
          console.log(e);
        },
      });
    },
    infiniteHandler($state) {
      const page = {
        page: app.page,
      };
      $.ajax({
        type: 'POST',
        url: '/api/get_messages_page',
        data: page,
        success: (messages) => {
          if (util.isFilledArray(messages)) {
            const msgs = messages.map((m) => this.processMessage(m, true, 0));
            app.messages.push(...msgs);
            app.page += 1;
            $state.loaded();
          } else {
            $state.complete();
          }
        },
        error(e) {
          console.log(e);
          $state.error();
        },
      });
    },
    infiniteHandlerSearch($state) {
      const data = {
        page: app.search_page,
        search_text: app.search_text,
      };

      $.ajax({
        type: 'POST',
        url: '/api/get_message_search_like',
        data,
        success: (messages) => {
          app.search_infinity_show = true;
          if (util.isFilledArray(messages)) {
            const msgs = messages.map((m) => this.processSearchMessage(m, data.search_text));
            app.search_messages.push(...msgs);
            app.search_page += 1;
            $state.loaded();
          } else {
            $state.complete();
          }
        },
        error(e) {
          console.log(e);
          $state.error();
        },
      });
    },
    logout() {
      $.ajax({
        type: 'post',
        url: '/api/logout',
        success: () => {
          socket.emit('disconnect');
          document.location.reload(true);
        },
        error: (e) => {
          console.log(e);
        },
      });
    },
    processMessage(msg, comment_show, comment_depth_indent) {
      const m = {
        id: msg.id,
        content: msg.content,
        my_message: msg.my_message,
        person_fio_short: msg.person.person_fio_short,
        childs: msg.childs || [],
        attached: msg.attached || [],
        child_count: msg.child_count || 0,
        child_parent_id: msg.parent_id || null,
        post_stamp: moment(msg.post_stamp).fromNow(),
        post_stamp_tooltip: moment(msg.post_stamp).format('DD.MM.YYYY HH:mm:ss'),
        change_stamp: msg.change_stamp ? moment(msg.change_stamp).fromNow() : null,
        change_stamp_tooltip: msg.change_stamp ? moment(msg.change_stamp).format('DD.MM.YYYY HH:mm:ss') : null,
        child_classes: ['message_post', 'list-message-item'],
        child_editor_attached: [],
        child_editor_content: '',
        child_editor_filepond: [],
        child_show: comment_show,
        depth_indent: comment_depth_indent,
        blink_newest: null,
        shared: null,
        shred_txt: null,
        // shared_expand: false,
        // shared_expandable: false,
        child_append_mode: false,
        child_editing_mode: false,
        avatar_link: `/avatar/${msg.person.avatar_link || 'no_avatar_30.png'}`,
        marked_state: msg.marked_state,
      };

      if (util.isFilledArray(msg.shared)) {
        m.shared = msg.shared;
        m.shared_txt = m.shared.slice(0, 3).map((x) => x.person_fio_short).join(', ');
        if (m.shared.length > 3) {
          const remainder = m.shared.length - 3;
          m.shared_txt += ` и ещё ${remainder} ${util.declensionOfNumbers(remainder, 'пользователь', 'пользователя', 'пользователей')}`;
          // m.shared_expandable = true;
        }
      }

      if (util.isFilledArray(m.attached)) {
        m.attached.forEach((f) => {
          f.link = `/api/get_attach/${m.id}/${f.id}`;
        });
      }

      if (util.isFilledArray(m.childs)) {
        m.childs = m.childs.map((x) => this.processMessage(x, false, comment_depth_indent + 1));
        m.child_count = m.childs.length;
      }

      if (m.child_parent_id) {
        m.child_classes = ['comment'];
        applyCommentClass(m, app.ratio);
      }

      return m;
    },
    getChildrenComments(msg_id, force = false, scrollTolast = false) {
      const comment_parent = util.findMessageById(app.messages, msg_id);
      if (!force && util.isFilledArray(comment_parent.childs)) {
        comment_parent.child_show = !comment_parent.child_show;
        return;
      }
      $.ajax({
        type: 'POST',
        url: '/api/get_message_comments',
        data: {
          parent_message_id: msg_id,
        },
        success: (msgs) => {
          if (util.isFilledArray(msgs)) {
            const messages = msgs.map((m) => this.processMessage(m, false, comment_parent.depth_indent + 1));
            comment_parent.childs = messages;
            comment_parent.child_count = messages.length;
            comment_parent.child_show = true;

            if (scrollTolast) {
              // todo на скрытой иерархии до сих пор не работает
              const last = messages.filter((m) => m.my_message).slice(-1)[0];
              last.blink_newest = (id) => {
                last.blink_newest = null;
                const message_div = $(`div[data-key='${last.id}']`);
                util.scrollInto(message_div[0], 'center');
                const bg_color = message_div.css('background-color');
                util.jqAnimateBlink(message_div, bg_color, 'hsl(50, 85%, 80%)', 700);
              };
            }
          }
        },
        error(e) {
          console.log(e);
        },
      });
    },
    resizeWindow() {
      const r = getRatioSize();
      if (this.ratio.availWidth !== r.availWidth) {
        this.ratio = r;
      }
    },
    // trumbowyg_focus(element) {
    //   let resize_callback =
    // },
  },
  created() {
    socket.on('append_comment', (comment) => {
      const parent_message = util.findMessageById(app.messages, comment.parent_id);

      if (parent_message) {
        const ready_message = app.processMessage(comment, false, parent_message.depth_indent + 1);
        parent_message.childs.push(ready_message);
        parent_message.child_count += 1;

        if (parent_message.child_show) {
          Vue.nextTick(() => {
            const message_div = $(`div[data-key='${ready_message.id}']`);
            if (ready_message.my_message) {
              util.scrollInto(message_div[0], 'center');
            }
            const bg_color = message_div.css('background-color');
            util.jqAnimateBlink(message_div, bg_color, 'hsl(50, 85%, 80%)', 700);
          });
        } else if (ready_message.my_message) {
          app.getChildrenComments(comment.parent_id, true, true);
        }
      }
    });

    socket.on('append_post', (msg) => {
      const m = this.processMessage(msg, true, 0);
      app.messages.unshift(m);
    });

    socket.on('update_post', (msg) => {
      const message = util.findMessageById(app.messages, msg.id);

      if (message) {
        if (util.isFilledArray(msg.attached)) {
          msg.attached.forEach((f) => {
            f.link = `/api/get_attach/${msg.id}/${f.id}`;
          });
        }
        message.content = msg.content;
        message.attached = msg.attached;
        message.change_stamp_tooltip = moment(msg.change_stamp)
          .format('DD.MM.YYYY HH:mm:ss');

        Vue.nextTick(() => {
          const message_div = $(`div[data-key='${message.id}']`);
          const bg_color = message_div.css('background-color');
          util.jqAnimateBlink(message_div, bg_color, 'hsl(50, 85%, 80%)', 700);
        });
      }
    });

    socket.on('delete_message', (msg) => {
      if (util.isFilledArray(app.messages) && msg) {
        if (msg.parent_id) {
          const parent = util.findMessageById(app.messages, msg.parent_id);
          const index = parent.childs.findIndex((m) => m.id === msg.id);
          if (index !== -1) {
            parent.childs.splice(index, 1);
            parent.child_count = parent.childs.length;
          }
          if (!util.isFilledArray(parent.childs)) {
            parent.child_count -= 1;
          }
        } else {
          const index = app.messages.findIndex((m) => m.id === msg.id);
          if (index !== -1) {
            app.messages.splice(index, 1);
          }
        }
      }
    });

    eventBus.$on('onresize', this.resizeWindow);
  },
  mounted() {
    $('#choose_shared').on('shown.bs.modal', () => {
      const el = $('#search_participant_id');
      el.focus();
    });
  },
});
