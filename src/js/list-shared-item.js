/* global $, Vue */

(function () {
  Vue.component('list-shared-item', {
    template: `
      <tr class="list-shared-item">
        <td class="cell-full cell-ellipsis">
          <span style="vertical-align: middle">
            <img :src="person.avatar_link" class="comment-avatar" alt="Аватар">
            <span :id="\`list-shared-item-txt-\${person.id}\`">
              {{ person.person_fio_full }}
            </span>
          </span>
        </td>
        <td class="cell-no-wrap">
          <button :class="['list-shared-item-btn', 'btn', person.isSelect ? 'btn-success' : 'btn-outline-secondary']"
                  @click="onToggleSelect">
            <i :class="['fa', person.isSelect ? 'fa-check' : 'fa-plus']"></i>
          </button>
        </td>
      </tr>
    `,
    props: {
      person: Object,
    },
    watch: {
      person: {
        handler(person) {
          this.$element.unmark({
            done: () => {
              if (person.search_text.length < 3) return;
              this.$element.mark(person.search_text, {
                element: 'span',
                accuracy: 'partially',
                className: 'search-mark',
                ignoreJoiners: true,
              });
            },
          });
        },
        deep: true,
      },
    },
    data() {
      return {
        $element: null,
      };
    },
    methods: {
      onToggleSelect() {
        this.person.isSelect = !this.person.isSelect;
        this.$emit('on-toggle-select', this.person);
      },
    },
    mounted() {
      this.$element = $(`span#list-shared-item-txt-${this.person.id}`);
      this.$element.mark(this.person.search_text, {
        element: 'span',
        accuracy: 'partially',
        className: 'search-mark',
        ignoreJoiners: true,
      });
    },
  });
}());
