/* ===========================================================
 * trumbowyg.unpasteimage.js v1.0
 * Basic base64 paste plugin for Trumbowyg
 * http://alex-d.github.com/Trumbowyg
 * ===========================================================
 * Author : Alexandre Demode (Alex-D)
 *          Twitter : @AlexandreDemode
 *          Website : alex-d.fr
 */

(function ($) {
  $.extend(true, $.trumbowyg, {
    plugins: {
      unPasteImage: {
        init(trumbowyg) {
          trumbowyg.pasteHandlers.push((pasteEvent) => {
            try {
              const items = (pasteEvent.originalEvent || pasteEvent).clipboardData.items;
              let mustPreventDefault = false;

              for (let i = items.length - 1; i >= 0; i -= 1) {
                if (items[i].type.match(/^image\//)) {
                  mustPreventDefault = true;
                }
              }

              if (mustPreventDefault) {
                pasteEvent.stopPropagation();
                pasteEvent.preventDefault();
              }
            } catch (c) {
            }
          });
        },
      },
    },
  });
}(jQuery));
